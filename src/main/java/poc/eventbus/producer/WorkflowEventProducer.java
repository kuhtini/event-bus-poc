package poc.eventbus.producer;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import poc.eventbus.common.model.WorkflowEvent;
import poc.eventbus.common.utils.PayloadGenerator;

@Component
@Slf4j
public class WorkflowEventProducer {

    private final JmsTemplate jmsTemplate;

    @Value("${event-bus.jms.topic.name}")
    String destination;

    public WorkflowEventProducer(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Scheduled(fixedRate = 1000)
    private void sendEvent() {
        WorkflowEvent event = WorkflowEvent.builder()
                .timeSpentMs(PayloadGenerator.getRandomTimeSpent())
                .payload(PayloadGenerator.getRandomPayload())
                .build();

        jmsTemplate.convertAndSend(destination, event);
        log.info("Sent Message to the Topic: {}, {}", destination, event);
    }
}
