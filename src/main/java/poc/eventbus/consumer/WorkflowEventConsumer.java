package poc.eventbus.consumer;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import poc.eventbus.common.model.WorkflowEvent;

import java.io.IOException;

@Component
@Slf4j
public class WorkflowEventConsumer {

    @Value("${event-bus.es.index.name}")
    String workflowEventIndex;

    private final ElasticsearchClient elasticsearchClient;

    public WorkflowEventConsumer(ElasticsearchClient elasticsearchClient) {
        this.elasticsearchClient = elasticsearchClient;
    }


    @JmsListener(destination = "${event-bus.jms.topic.name}")
    public void consume(WorkflowEvent event) throws IOException {
        log.info("Received Message from the topic: {}", event);
        elasticsearchClient.index(builder -> builder.index(workflowEventIndex).document(event));
        log.info("Sent to Elastic");
    }

}
