package poc.eventbus.common.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
public class WorkflowEvent {

    @Builder.Default
    private String traceId = String.valueOf(UUID.randomUUID());
    @Builder.Default
    private LocalDateTime creationDate = LocalDateTime.now();
    private String payload;
    private long timeSpentMs;
}
