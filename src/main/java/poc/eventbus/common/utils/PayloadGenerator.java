package poc.eventbus.common.utils;

import lombok.experimental.UtilityClass;

import java.util.concurrent.ThreadLocalRandom;

@UtilityClass
public class PayloadGenerator {

    public String getRandomPayload() {
        return switch (ThreadLocalRandom.current().nextInt(0, 3)) {
            case 1 -> "IPU processing";
            case 2 -> "Preprocessor";
            default -> "PdfPlatform";
        };
    }

    public long getRandomTimeSpent() {
        return ThreadLocalRandom.current().nextInt(50, 1500);
    }

}
